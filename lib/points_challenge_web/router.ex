defmodule PointsChallengeWeb.Router do
  use PointsChallengeWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PointsChallengeWeb do
    pipe_through :api

    get "/", UserController, :index
  end
end
