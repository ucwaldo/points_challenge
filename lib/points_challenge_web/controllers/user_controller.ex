defmodule PointsChallengeWeb.UserController do
  use PointsChallengeWeb, :controller

  alias PointsChallenge.UsersPointManager

  def index(conn, _params) do
    case UsersPointManager.list_users() do
      {users, timestamp} when is_list(users) ->
        render(conn, "index.json", users: users, timestamp: timestamp)

      _ ->
        conn
        |> put_view(ErrorView)
        |> put_status(:internal_server_error)
        |> render("500.json")
    end
  end
end
