defmodule PointsChallenge.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children =
      :points_challenge
      |> Application.get_env(:minimal, false)
      |> get_children()

    opts = [strategy: :one_for_one, name: PointsChallenge.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    PointsChallengeWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp get_children(_minimal = true) do
    [
      PointsChallenge.Repo
    ]
  end

  defp get_children(_minimal) do
    [
      PointsChallenge.Repo,
      PointsChallenge.UsersPointManager,
      PointsChallengeWeb.Telemetry,
      PointsChallengeWeb.Endpoint
    ]
  end
end
