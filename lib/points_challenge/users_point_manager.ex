defmodule PointsChallenge.UsersPointManager do
  @moduledoc """
  GenServer holding state of max_number and last request timestamp: {max_number, timestamp}.

  Periodically triggers update of points on all user records and handles call to retrieve users.

  The interval of the periodic update can be configured via the environment variable `UPDATE_INTERVAL_MIN`
  and is read from `config/runtime.exs`.

  The default interval is 1 minute.
  """

  use GenServer

  alias PointsChallenge.Users

  require Logger

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, {random_number(), nil}, name: __MODULE__)
  end

  # Api

  def list_users do
    GenServer.call(__MODULE__, :list_users_exceeding_max_number)
  end

  # Callbacks

  @impl true
  def init(state) do
    :timer.send_interval(periodic_update_interval(), :update_state_and_user_points)

    Logger.info("Starting UsersPointManager with initial state: #{inspect(state)}")

    {:ok, state}
  end

  @impl true
  def handle_call(:list_users_exceeding_max_number, _from, {max_number, timestamp}) do
    users = Users.list_users_exceeding_points(max_number)
    new_timestamp = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    {:reply, {users, timestamp}, {max_number, new_timestamp}}
  end

  @impl true
  def handle_info(:update_state_and_user_points, {_max_number, timestamp}) do
    new_state = {random_number(), timestamp}
    Logger.info("Updating UsersPointManager state: #{inspect(new_state)}")

    Task.start(&Users.update_all_user_points/0)

    {:noreply, new_state}
  end

  defp random_number do
    Enum.random(0..100)
  end

  defp periodic_update_interval do
    interval = Application.fetch_env!(:points_challenge, :update_interval_min)
    Logger.info("UsersPointManager update interval: #{interval} min.")
    :timer.minutes(interval)
  end
end
