defmodule PointsChallenge.Release do
  @app :points_challenge

  def migrate do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  def seed do
    load_app()

    [repo | _] = repos()

    {:ok, _, _} =
      Ecto.Migrator.with_repo(repo, fn _repo ->
        @app
        |> Application.app_dir("priv/repo/seeds.exs")
        |> Code.eval_file()
      end)
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.put_env(@app, :minimal, true)
    Application.load(@app)
  end
end
