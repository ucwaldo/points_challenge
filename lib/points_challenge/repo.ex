defmodule PointsChallenge.Repo do
  use Ecto.Repo,
    otp_app: :points_challenge,
    adapter: Ecto.Adapters.Postgres
end
