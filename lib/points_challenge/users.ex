defmodule PointsChallenge.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false

  alias PointsChallenge.Repo
  alias PointsChallenge.User

  @doc """
  Returns the list of 0, 1 or 2 users with points exceeding max_number.

  The default limit can be increased providing a second parameter.

  ## Examples

  iex> list_users_exceeding_points(50)
  [%User{}, ...]

  """
  def list_users_exceeding_points(max_number, limit \\ 2) do
    User
    |> where([u], u.points > ^max_number)
    |> limit(^limit)
    |> Repo.all()
  end

  @doc """
  Updates all users in bulks setting random `points`values.

  Additionally updates `updated_at` column with the current time.

  ## Examples

  iex> update_all_user_points()
  :ok

  """
  def update_all_user_points do
    batch_size = 100_000

    User
    |> select([u], u.id)
    |> Repo.all()
    |> Enum.chunk_every(batch_size)
    |> Enum.each(fn user_ids ->
      timestamp = NaiveDateTime.utc_now()

      from(u in User,
        where: u.id in ^user_ids,
        update: [set: [points: fragment("floor(random() * 101)"), updated_at: ^timestamp]]
      )
      |> Repo.update_all([])
    end)
  end
end
