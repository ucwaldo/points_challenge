# Script for populating the database. You can run it as:
#
# mix run priv/repo/seeds.exs

alias PointsChallenge.Repo

user_count = 1_000_000
batch_size = 80_000

# https://klotzandrew.com/blog/postgres-passing-65535-parameter-limit
unnest_bulk_insert =
  "INSERT INTO users (inserted_at, updated_at) (SELECT * FROM UNNEST($1::timestamp[], $2::timestamp[]))"

1..user_count
|> Enum.chunk_every(batch_size)
|> Enum.each(fn rows ->
  timestamp = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  timestamps = Enum.map(rows, fn _ -> timestamp end)
  Ecto.Adapters.SQL.query!(Repo, unnest_bulk_insert, [timestamps, timestamps])
end)
