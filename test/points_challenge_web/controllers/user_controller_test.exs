defmodule PointsChallengeWeb.UserControllerTest do
  use PointsChallengeWeb.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"users" => [], "timestamp" => _} = json_response(conn, 200)
    end
  end
end
