defmodule PointChallenge.UsersTest do
  use PointsChallenge.DataCase

  alias PointsChallenge.Users

  import PointsChallenge.UsersFixture

  test "list_users_exceeding_points/2 returns users exceeding max_number" do
    user1 = user_fixture(%{points: 60})
    user2 = user_fixture(%{points: 80})

    assert Users.list_users_exceeding_points(50) == [user1, user2]
    assert Users.list_users_exceeding_points(60) == [user2]
  end

  test "list_users_exceeding_points/2 returns no more than two users" do
    user_fixture(%{points: 10})
    user_fixture(%{points: 60})
    user_fixture(%{points: 80})

    assert [_, _] = Users.list_users_exceeding_points(5)
  end

  test "update_all_user_points/2 successfully updates points of all users" do
    user_fixture(%{points: 10})
    user_fixture(%{points: 60})
    user_fixture(%{points: 80})

    assert :ok = Users.update_all_user_points()
  end
end
