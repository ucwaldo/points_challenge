defmodule PointsChallenge.UsersPointManagerTest do
  use PointsChallenge.DataCase

  alias PointsChallenge.UsersPointManager

  import PointsChallenge.UsersFixture

  setup do
    user_fixture(%{points: 20})
    user_fixture(%{points: 50})
    :ok
  end

  test "UsersPointManager.handle_call/3 :: :list_users_exceeding_max_number" do
    state = {30, nil}
    response = UsersPointManager.handle_call(:list_users_exceeding_max_number, nil, state)

    assert {:reply, {[_], nil}, {30, %NaiveDateTime{}}} = response
  end
end
