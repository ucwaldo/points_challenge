defmodule PointsChallenge.UserTest do
  use PointsChallenge.DataCase, async: true
  alias PointsChallenge.User

  test "is valid without attributes" do
    changeset = User.changeset(%User{}, %{})
    assert %Ecto.Changeset{valid?: true} = changeset
  end

  test "points can be any integer between 0 and 100" do
    changeset = User.changeset(%User{}, %{points: 50})
    assert %Ecto.Changeset{valid?: true} = changeset
  end

  test "points can not be below 0" do
    changeset = User.changeset(%User{}, %{points: -1})
    assert %Ecto.Changeset{valid?: false} = changeset
  end

  test "points can not exceed 100" do
    changeset = User.changeset(%User{}, %{points: 101})
    assert %Ecto.Changeset{valid?: false} = changeset
  end
end
