defmodule PointsChallenge.UsersFixture do
  @moduledoc """
  This module defines test helpers for creating users.
  """

  alias PointsChallenge.Repo
  alias PointsChallenge.User

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      %User{}
      |> User.changeset(attrs)
      |> Repo.insert()

    user
  end
end
