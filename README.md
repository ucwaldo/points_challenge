# PointsChallenge

This is the result of a coding challenge for a "Senior Backend Engineer - Elixir" position at Remote. \
The requirements are laid out [here](https://www.notion.so/Backend-code-exercise-d7df215ccb6f4d87a3ef865506763d50).

- [Development setup](#development-setup)
- [Development journal](#development-journal)

The application starts a GenServer that periodically updates all users points
and holds some internal state consisting of a randomly generated `max_number` as well
as the last request `timestamp`. The GenServer serves as the api to list a limited amount of
users that exceed the `max_number` and updates the last request `timestamp` whenever the api
function gets called from a controller endpoint that is served from the root http path.

The update interval can be changed via the following environment variable when the
app starts and defaults to 1 minute: `UPDATE_INTERVAL_MIN=2 mix phx.server`

There is an `.iex.exs` that aliases the `Users` context and the `UsersPointManager`
GenServer for easy access in an iex session.

The application should be capable of handling more than a million user records,
so feel free to increase the `user_count` in `priv/repo/seeds.exs` or to rerun the
seed before starting the Phoenix server.

I really enjoyed working on this challenge, given the scope wasn't too large and it included
a couple of interesting challenges that required some iterations.

## Development setup

Note that the Erlang/OTP and Elixir versions you use in development should match
with the versions you intend to use in the production environment.

### For the impatient

```
$ asdf install
$ mix deps.get
$ mix ecto.setup
$ mix test
$ iex -S mix phx.server
```

### Installing Erlang & Elixir

I strongly recommend using [asdf](https://asdf-vm.github.io/asdf/#/core-manage-asdf-vm).
This tool allows you to manage multiple languages on different versions via the concept of
[plugins](https://asdf-vm.com/#/core-manage-plugins).

With `asdf` installed, cd into the root directory of the repo and run `asdf install`,
which will install the language dependencies inferred from the `.tool-versions` file.

Once installed, `elixir -v` should print something similar to this:

```
$ elixir -v
Erlang/OTP 24 [erts-12.2.1] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [jit]

Elixir 1.13.3 (compiled with Erlang/OTP 24)
```

If you installed a version of Elixir for the first time, hex and rebar need to be
installed once and updated from time to time:

```
$ mix local.hex --force
$ mix local.rebar --force
```

### Installing prerequisites

For the dependency `postgresql` just follow the installation instructions for your favorite package manager:

##### apt (Linux)

```
$ apt install postgresql
```

##### homebrew (mac OS)

```
$ brew install postgres
```

### Getting started

Install and compile the dependencies:

```
$ mix do deps.get, compile
```

Finally, setup the database, run the migrations and seed the database:

```
$ mix do ecto.create, ecto.migrate, run priv/repo/seeds.exs
```

_or_ use the setup task which does all of the above:

```
$ mix ecto.setup
```

You can now start the application:

```
$ iex -S mix phx.server
iex>
```

Following the requirements, the default Endpoint port is set to `3000`.
With the app started, open a new shell and try calling the app:

```
$ curl localhost:3000
```

## Development journal

### 1) Project setup

We want to setup a Phoenix application serving a single endpoint that returns JSON.
Hence, I went with all flags that remove a lot of the commodities one gets when
creating a new Phoenix application and kept everything as slim as possible:

```
mix phx.new points_challenge --no-assets --no-html --no-live --no-dashboard --no-mailer
```

I added exactly one additional dependency, [`Credo`](https://hex.pm/packages/credo) for static code analysis.
It helps a lot to keep things consistent and is a great library that shouldn't be missed
in any Elixir project in my humble opinion. There is no setup cost and it's highly customizable
to suit an individuals, teams or companies need without much overhead.

```
$ mix credo --strict
```

Finally, I removed the logger format configuration from `config/dev.exs` so we can
see log timestamps while running the app in `MIX_ENV=dev`.

### 2) Creating the user model

Phoenix generators are very powerful and I could have used the `phx.gen.json` generator
to prepare some code for the later steps. However, we wouldn't need most of the generated code since
we don't plan on creating or updating user structs via api. I went with `phx.gen.schema`
which generates a migration file as well as an ecto schema module for the users
data we'll be dealing with.

```
$ mix phx.gen.schema User users points:integer
```

Ecto takes care of creating the two timestamp columns `updated_at` and `inserted_at` as long as
they are specified in the migration and schema file, which is the case by default.

The only things I had to change was adding the default points value in the migration file (so the default is enforced at database level)
as well as the default points value in the Ecto schema (so the default is present when dealing with unpersisted `User` structs).
Knowing I'll have to query users by points, I created an index on the `points` column
in the same migration file that the generator created.

Additionally, I kept the points value `required` and validate it to be an integer value
between 0 and 100 in the changeset method, as specified in the requirements, even though
we won't be dealing with changesets in the course of this challenge.

Lastly I've added some basic tests that assert the behaviour described above in `test/points_challenge/user_test.exs`.

### 3) Seeding the database

This was fun and I learned something new. Given the requirement of seeding the database with 1\_000\_000 user records,
I knew I should approach the import in bulks to not overload the database. Since I wanted to have the timestamp
columns set with the seed, I had to explicitly pass in those values, as Ecto intentionally does not set or update those
columns when using `Ecto.Repo` methods like `insert_all/3` or `update_all/3`.

I was quite happy with my first iteration doing a bulk insert with batches of 33_000 user records,
being limited by the Postgres parameter limit at `65_535`. It took about ~6 seconds and
was perfectly fine to meet the initial requirement, but that parameter limit triggered me to dig a bit deeper
and see if I could make it faster. After doing some research I discovered [this article](https://klotzandrew.com/blog/postgres-passing-65535-parameter-limit)
effectively explaining a "hack" on how to bypass the parameter limit using the Postgres
array method `UNNEST`, allowing to expand the `INSERT` values from a subsequent `SELECT`.

This drastically improved the performance and the time needed to seed the database remained constantly under
2 seconds (~1.8s) on my four year old development machine. I found the sweet spot to be to insert in
bulks of 80\_000. Surprisingly enough, this allowed me to insert all 1 million records with a single INSERT statement,
but that wasn't faster than doing it in larger bulks and would certainly put more pressure on the database.

Thanks to Ecto's great support for raw SQL this was pretty simple to implement and actually
reduced the lines of code necessary while maintaining decent readability and not obfuscate what's happening in `seeds.exs`.

Here some timed output from running the seed with the first iteration:

```
$ time mix run priv/repo/seeds.exs
mix run priv/repo/seeds.exs  5.89s user 0.94s system 60% cpu 11.288 total
```

and with the second iteration, leveraging Postgres `UNNEST` array method:

```
$ time mix run priv/repo/seeds.exs
mix run priv/repo/seeds.exs  1.85s user 0.34s system 56% cpu 4.125 total
```

### 4) Users context module

With the seed out of the way and (at least) a million beautiful user records in our Postgres database table,
I decided it was a good time to tackle the two database queries we'll use in the following GenServer implementation.

For that I created a `Users` context module whos sole job is to formulate and execute the two Ecto queries we need:

#### 4.1) List users with points exceeding given number

This was pretty straight forward to implement and quite performant from the get go
thanks to the index on `points` that we've created in step [2](#2-creating-the-user-model)). Naming is and remains a difficult task,
especially for such a "weird" coding challenge (cited from the requirements ;)). I finally settled on `list_users_exceeding_points/2`
which receives a `max_number` as the first argument and an optional second argument that allows us
to change the default limit in case someone ever wanted to return more than 2 users. It wasn't in the requirements
and I intentionally decided to not support it with the endpoint that we'll create later on.

<span id="42-update-all-users-points"></span>
#### 4.2) Update _all_ user records setting random points between 0 and 100

On first sight, this was also straight forward to implement and it was great to see
that a plain `Repo.update_all/3` on 1 million records took no longer than 4 seconds while not even blocking
incoming reads. This was good enough to meet the challenge requirements, but again,
triggered me to see if I could take it a little step further without overdoing it.

So, with 1 million users taking ~4 seconds to update, what happens if I had 4 million users, or more in the database?
The query would take more than 15 seconds, not only putting quite a lot of pressure on the database, but also exceeding
Postgres default timeout of 15 seconds. The query would fail exactly with that, a timeout.

To address those things I went with bulk queries again. People who love databases would probably write a remote procedure
(which i did for the fun of it), but given I am applying for an Elixir position, I of course implemented it with Ecto and settled with it.

It's easy to fall in the trap of inferring ids from the record count and iterate through that,
potentially formulating queries that update no records at all or, even worst, the wrong records.
To avoid this I knew I should first get a list of user ids from the database, chunk the list into batches that I want to iterate through
and create a timestamp to correctly update the `updated_at` column (remember, Ecto `*_all` methods dont have the convenience of doing this for us), next to the random `points` values.

I settled with a batch size of 100\_000 user ids to update, as that seemed easy enough on the database, taking about ~550ms per `UPDATE`,
and finally allowed me to deal with far more than 1 million records in the database, not running into timeouts nor
putting enough pressure on the database that could eventually block incoming reads.

One caveat that is important to note is that while dealing with multiple millions of records, it's not unlikely that updating in bulks
takes longer than our interval span. Once that becomes a problem, one approach could be to implement some kind of lock mechanism
that prevents updates from overlapping which is however out of scope of this challenge.

### 5) GenServer setup

Now it was time to tackle the GenServer and thanks to the groundwork I could keep it rather slim.
For easy access to the API, it's started with the `name` option. It's initialized with the specified initial state,
a tuple consisting of a randomly generated integer between 0 and 100 as the first element (`max_number`) and `nil`
(the last request `timestamp`) as the second element: `{max_number, timestamp}`.
If we held at least two more values in the GenServer state, I would probably go with a map instead of a tuple for readabilities sake.

The `init/1` callback initiates the periodic feature that updates all users `points` to
new random integers ([4.2](#42-update-all-users-points))
as well as updates the internal GenServer state with a new random `max_number`.
The interval is configurable but defaults to be exactly one minute as specified in the requirements.
Configuration is read in `config/runtime.exs` and can be changed via environment variable, making it easy to change the interval depending on
the environment the application is started in. That is quite a common requirement and didn't cost a lot of extra effort.

There are quite a few ways to impelement a periodic feature with Elixir, and many more libraries that
come with mighty scheduling features, such as Oban or Quantum, which are completely out of scope for our requirement, but always worth a mention.

Our periodic feature is initiated with erlangs `:timer.send_interval/2` function, which
takes the interval in milliseconds as a first argument and the message name of the `handle_info` callback that we want to periodically execute as a second argument.
The only real downside to this approach is that we can't change the interval at runtime _after_ the application was started,
while this would be possible if we used an approach using `Process.send_after/4` where every execution re-schedules the next iteration with the given interval.

A note on testing here, while perfectly possible, testing GenServers can become quite cumbersome. 
[This](https://samuelmullen.com/articles/elixir-processes-testing/) is a great article discussing
some of the approaches as well as pros and cons.
I did the bare minimum here, but did not want to adapt my implementation for the sake of testing in this coding challenge.
That doesn't mean I would generally not go the extra mile to test GenServers in an isolated manner whenever it makes sense.

#### 5.1) Update state and user points callback

Calls `Users.update_all_user_points/0` within a new process spawned from `Task.start/1`.
This is necessary to keep Ecto from blocking our GenServer process and to be able to reply from our callback without delay.
Finally, we generate a new random number, log the new state and return from the callback with `{:noreply, new_state}` updating the GenServer state.

The caveat here is that updating those records is not idempotent, meaning when the update fails or the application restarts,
we can not pick off where it stopped but will start the update from scratch. There are a couple ways to tackle this,
like keeping track of the last updated id while processing the batches, which comes with its own caveats and is also not in the scope of this
challenge, nevertheless it is important to be aware of these things when operating in dynamic environments.

#### 5.2) List users exceeding max\_number callback

Calls `Users.list_users_exceeding_points/1` and replies with a two element tuple, consisting of the query result,
a list of 0, 1 or 2 users as the first element and the last request timestamp as the second element.
Additionally, the GenServer state is being updated, setting the current time as the last request timestamp.

This callback  is invoked from the single API function our GenServer exposes: `list_users/0` which will be consumed by our controller Endpoint, which comes next.

It's important to note that our GenServer state does not survive application restarts,
which could be taken care of by persisting the `max_number` as well as the last request `timestamp` and considering those upon GenServer or application restarts.

### 6) The Endpoint

This last requirement consists of a controller module, with a single action that consumes our GenServer api and returns some JSON with a list of users next to the last request timestamp:

```
$ curl 'http://localhost:3000'
{"timestamp":"2022-04-30T13:26:18","users":[{"id":100002,"points":95},{"id":100003,"points":96}]}
```

It renders the JSON from a dedicate View module and handles any error that we might receive from the GenServer
by returning with an http status code 500 and an unhelpful error message.  Finally,
I added some basic tests for the controller action as well as the view.

Note that I would usually avoid exposing database record ids from an endpoint unless there is a very good reason to do so.

### 7) Closer to production

The challenge requirement mentions "assume this code would be put in production". I did not take that literally,
but wanted to show how simple it can be to create a local production release. I've added an env file
and a release module which allows us to execute migrations from the Elixir release and start it.

Before moving on, take a look at the `.env.local` file and assert that you can establish
a successful Postgres connection with that postgres url.  If you ran the app in development mode
everything should be fine as is.

Let's create a "production" release, run the migrations and start the release following these steps:

```
$ MIX_ENV=prod mix release

$ env $(cat .env.local | xargs) _build/prod/rel/points_challenge/bin/points_challenge eval 'PointsChallenge.Release.migrate()'

$ env $(cat .env.local | xargs) _build/prod/rel/points_challenge/bin/points_challenge eval 'PointsChallenge.Release.seed()'

$ env $(cat .env.local | xargs) _build/prod/rel/points_challenge/bin/points_challenge start
```

Ignore the logline about access, we did not hassle with creating a self-signed certificate
nor a reverse proxy that would allow us to consume the endpoint at ~~https://localhost~~.
At the same time I saw no good reason to change those sane production defaults. There are many
more considerations to make before putting an application to production.

But being able to locally create a production release without much effort is pretty cool in my opinion.

Note that the endpoint is now listening on port `3010`:

```
$ curl 'http://localhost:3010'
{"timestamp":null,"users":[{"id":100002,"points":98},{"id":100008,"points":87}]
```
