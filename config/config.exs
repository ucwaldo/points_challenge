import Config

config :points_challenge,
  ecto_repos: [PointsChallenge.Repo]

config :points_challenge, PointsChallengeWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: PointsChallengeWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: PointsChallenge.PubSub,
  live_view: [signing_salt: "7md8hvfW"]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

import_config "#{config_env()}.exs"
