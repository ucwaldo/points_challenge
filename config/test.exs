import Config

config :points_challenge, PointsChallenge.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "points_challenge_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :points_challenge, PointsChallengeWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "Y4cxGBqXV8clNU1MDr1n7qNcm0R4G0LLXK8Ny8FL4O4AC/+6DBurOU2v0ZlfeXwT",
  server: false

config :logger, level: :warn

config :phoenix, :plug_init_mode, :runtime
