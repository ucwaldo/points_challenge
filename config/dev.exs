import Config

config :points_challenge, PointsChallenge.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "points_challenge_dev",
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :points_challenge, PointsChallengeWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 3000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "BwbSTIad275NsQsjvd00AOdAoBMUfqQjiz2vk9UbvTK33RCa9ryJX3NQVZbsBSKs",
  watchers: []

config :phoenix, :stacktrace_depth, 20

config :phoenix, :plug_init_mode, :runtime
